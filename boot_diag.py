#!/usr/bin/env python3

import sys
import argparse

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

__version__ = '0.1.0'

def validate(rules):
    """Validate the rules file"""
    return [rules.getroot().tag == 'ruleset', # the root tag must 'ruleset'
            (rules.find("rule[@id='{}']".format(rules.getroot().attrib['first'])) is not None) if rules.find('[@first]') else True]

def parse(rules):
    RootRule = rules.getroot()
    CurrentRule = rules.find("rule[@id='{}']".format(RootRule.attrib['first'])) if validate(rules)[1] else rules.find('rule')
    for m in list(RootRule.find("metadata")):
        print(f"{m.tag:<19}: {m.text}")
    count = 0
    # first run or get link
    while CurrentRule.find('condition') is not None:
        count += 1
        do = input(f"[{count:>3}] {CurrentRule.find('condition').text} [Y/n] ") or 'Y'
        CurrentRule = CurrentRule.find('else') if do[0].lower() == 'n' else CurrentRule.find('do')
        alink = CurrentRule.find('link')
        if alink is not None:
            CurrentRule = RootRule.find(".//rule[@id='{}']".format(alink.attrib['href'].lstrip('#')))
            continue

        # second run or after go to link
        while CurrentRule.find('rule/condition') is not None:
            count += 1
            do = input(f"[{count:>3}] {CurrentRule.find('rule/condition').text} [Y/n] ") or 'Y'
            CurrentRule = CurrentRule.find('rule/else') if do[0].lower() == 'n' else CurrentRule.find('rule/do')
            # link to other rule
            alink = CurrentRule.find('link')
            if alink is not None:
                # print("\033[39m[INLINK]", CurrentRule, 'to', alink.attrib['href'])
                # wait = alink.attrib['wait'] if alink.attrib['wait'] is not None else 'false'
                # if wait == 'true':
                #     go = input(f"[!:>3] {alink.text} [Y/n] ") or 'Y'
                #     if go[0].lower() == 'n':
                #         print('\n[END]\n')
                #         sys.exit(0)
                #     else: pass
                # elif wait == 'false':
                CurrentRule = RootRule.find(".//rule[@id='{}']".format(alink.attrib['href'].lstrip('#')))
                break
            # end of rule
            elif CurrentRule.find('set') is not None:
                print(f"[ ! ] {CurrentRule.find('set').text}")
                return
            else:
                continue
        if CurrentRule.find('set') is not None:
            print(f"[ ! ] {CurrentRule.find('set').text}")
            return

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--rules', nargs='?', dest='r', metavar='rule file', help='rules file in xml format')
    parser.add_argument('-v', '--validate', action='store_true', dest='v', help='only validate the rules file and exit')
    parser.add_argument('-V', '--version', action='store_true', dest='V', help='just show version and exit')

    if len(sys.argv) == 1:
        parser.print_help(sys.stdout)
        sys.exit(1)
    args = parser.parse_args()

    if args.V:
        print(__version__)
        sys.exit(0)

    if args.r is None:
        print('Please provide the rules file', file=sys.stderr)
        sys.exit(1)

    ruleset = ET.ElementTree(file=args.r)

    valid = all(validate(ruleset))
    if not valid:
        print('The rules file is not valid')
        sys.exit(2)
    if valid:
        if args.v:
            print('The rules file is valid')
            sys.exit(0)

        parse(ruleset)
            