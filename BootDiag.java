import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.xpath.XPathException;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

class BootDiag {
    public static void main(String[] argv) {
        String RulefName = ((argv.length == 0) || argv[0].equals("")) ? "rule.xml" : argv[0];
        BufferedReader bIn = new BufferedReader(new InputStreamReader(System.in));

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document RootRule = db.parse(RulefName);
            XPath xPath = XPathFactory.newInstance().newXPath();

            String pRoot = "/ruleset";
            String pmFR = pRoot + "/@first";
            String pCurrentRule = ((boolean) xPath.compile(pmFR).evaluate(RootRule, XPathConstants.BOOLEAN)) ? String.format("//rule[@id='%s']", (String) xPath.compile(pmFR).evaluate(RootRule, XPathConstants.STRING)) : "/rule[1]";

            NodeList Meta = (NodeList) xPath.compile(pRoot+"/metadata/child::*").evaluate(RootRule, XPathConstants.NODESET);
            for (int i=0; i<Meta.getLength(); i++) {
                Node cMeta = Meta.item(i);
                System.out.printf("%-17s: %s%n", cMeta.getNodeName(), cMeta.getTextContent()); // print metadata
            }
            System.out.println();
            // TODO: more memory-efficient
            Node CurrentRule = (Node) xPath.compile(pRoot + pCurrentRule + "/ancestor::*").evaluate(RootRule, XPathConstants.NODE);
            String NextRoot; // next rule

            int Count = 0;
            do {
                if ((boolean) xPath.compile("./link").evaluate(CurrentRule, XPathConstants.BOOLEAN)) {
                    String pLink = (String) xPath.compile("./link/@href").evaluate(CurrentRule, XPathConstants.STRING);
                    pCurrentRule = String.format("//rule[@id='%s']", pLink.replaceAll("[#]", ""));
                    CurrentRule = (Node) xPath.compile(pRoot + pCurrentRule + "/ancestor::*").evaluate(RootRule, XPathConstants.NODE);
                }
                Count++;
                System.out.printf("[%3d] %s [Y/n] ", Count, xPath.compile(pCurrentRule + "/condition/text()").evaluate(CurrentRule, XPathConstants.STRING));
                NextRoot = bIn.readLine();
                NextRoot = NextRoot.trim().toLowerCase();
                if ("".equals(NextRoot) || "yes".equals(NextRoot) || "ya".equals(NextRoot) || "y".equals(NextRoot)) {
                    NextRoot = "/do";
                } else if ("no".equals(NextRoot) || "n".equals(NextRoot)) {
                    NextRoot = "/else";
                } else { continue; /* ulang */}
                CurrentRule = (Node) xPath.compile(pCurrentRule + NextRoot).evaluate(CurrentRule, XPathConstants.NODE);
                pCurrentRule = "rule";
            } while (! ((boolean) xPath.compile("./set").evaluate(CurrentRule, XPathConstants.BOOLEAN)));
            System.out.printf("%n[ ! ] %s%n", xPath.compile("./set/text()").evaluate(CurrentRule, XPathConstants.STRING)); // end (set)
        } catch (XPathException e) {
            System.err.printf("XPE: %s%n", e.toString());
        } catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
    }
}